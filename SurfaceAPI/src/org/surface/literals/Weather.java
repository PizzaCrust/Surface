package org.surface.literals;
/**
 * Represents weather
 * @author PizzaCrust
 *
 */
public enum Weather {
	CLEAR,
	DOWNFALL,
}
