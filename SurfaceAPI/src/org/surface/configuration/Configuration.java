package org.surface.configuration;

import java.io.File;

import org.surface.exceptions.ConfigurationError;

/**
 * Represents a YAML configuration file.
 * @author PizzaCrust
 *
 */
public interface Configuration {
	/**
	 * Saves a configuration to a file
	 * @param file the file that configuration will be saved to
	 * @throws ConfigurationError can throw if something went wrong
	 */
	void save(File file) throws ConfigurationError;
	/**
	 * Loads a configuration from a file
	 * @param file the file that will be loaded
	 * @throws ConfigurationError can throw if something went wrong
	 */
	void load(File file) throws ConfigurationError;
	/**
	 * Sets a value to a path inside of configuration
	 * @param path the path to the value
	 * @param object the value
	 */
	void set(String path, Object object);
	/**
	 * Gets a value of a path
	 * @param path the path of the value
	 * @param incase if the value doesn't exist this value will be used
	 * @return the value of the path
	 */
	Object get(String path, String incase);
	/**
	 * Sets a header of the configuration
	 * @param header the header
	 */
	void setHeader(String header);
}
