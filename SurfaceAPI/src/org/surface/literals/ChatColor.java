package org.surface.literals;

/**
 * Basic representations of colors in chat.
 * @author PizzaCrust
 *
 */
public enum ChatColor {
	AQUA,
	BLACK,
	BLUE,
	BOLD, 
	DARK_AQUA,
	DARK_BLUE,
	DARK_GRAY,
	DARK_GREEN,
	DARK_PURPLE,
	DARK_RED,
	GOLD,
	GRAY,
	GREEN,
	ITALIC,
	LIGHT_PURPLE,
	MAGIC,
	RESET,
	RED,
	STRIKETHROUGH,
	UNDERLINE,
	WHITE,
	YELLOW,
}
