# Contributing
Interested in contributing to this API?
We are always in need of developers to further improve the API, here's
how you can contribute.

1. Check the Issues page and see what Issues you can help with.

2. After deciding what issue you can contribute to, make sure you have a GitLab account.

3. Clone the repository from this link: https://gitlab.com/PizzaCraft/Surface.git

4. Do your stuff.

5. Send a pull request containing your edits.

Thanks, and we hope you enjoy creating and improving our server API.