# Surface
A minecraft plugin API aiming to compete with SpongePowered.  
**Currently being rewritten and redesigned as a abstracted API.**  
  
View our website at: http://surfaceapi.weebly.com/

Surface Implementations have been moved to the Wiki. Please view the Wiki to see the Surface Implementations.

Please note that this is a API not a implementation, no functionality has been given to this API yet.

## Examples
**Examples are not functional because this only API code which has no functionality.**  
Here's a example creating a plugin.  
```
public class Example implements SurfacePlugin {
	@Override
	public void commenced(SurfaceInitializationEvent e){
		System.out.println("Yay! Plugin is starting!")
	}
	@Override
	public void conclude(){
		System.out.println("Noo.. Plugin is concluding...")
	}
}
```