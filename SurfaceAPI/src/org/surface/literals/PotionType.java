package org.surface.literals;

/**
 * Enum for Potions
 * @author PizzaCrust
 *
 */
public enum PotionType {
	FIRE_RESISTANCE,
	STRENGTH,
	HEALING,
	LEAPING,
	INVISIBLITY,
	REGENERATION,
	SWIFTNESS,
	NIGHT_VISION,
	WATER_BREATHING,
	HARMING,
	WEAKNESS,
	SLOWNESS,
	POSION,
}
