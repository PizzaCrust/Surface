package org.surface.literals;

/**
 * Enums for GameModes
 * @author PizzaCrust
 *
 */
public enum GameMode {
	CREATIVE,
	SURVIVAL,
	ADVENTURE,
	SPECTATOR,
}
