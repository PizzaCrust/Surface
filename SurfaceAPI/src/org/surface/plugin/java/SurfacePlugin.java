package org.surface.plugin.java;

import org.surface.plugin.events.SurfaceInitializationEvent;
/**
 * Represents a SurfacePlugin.
 * @author PizzaCrust
 *
 */
public interface SurfacePlugin {
	/**
	 * Executes when plugin is enabled
	 * @param e the SurfaceInitializationEvent
	 */
	void commenced(SurfaceInitializationEvent e);
	/**
	 * Executes after the plugin is disabled
	 */
	void conclude();
	/**
	 * Gets if the plugin is enabled
	 * @return a boolean containing if the plugin is enabled
	 */
	boolean isEnabled();
}
