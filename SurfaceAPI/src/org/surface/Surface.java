package org.surface;

import java.io.File;
import java.util.UUID;

import org.surface.entity.Player;
import org.surface.logging.Logger;
import org.surface.world.World;

/**
 * Represents the server instance in Surface.
 * @author PizzaCrust
 *
 */
public interface Surface {
	/**
	 * Broadcasts a message throughout the server (includes console) 
	 * @param message the message that will be broadcasted
	 */
	void broadcastMessage(String message);
	/**
	 * Gets the Surface API version
	 * @return a string containing the API version
	 */
	String getSurfaceVersion();
	/**
	 * Gets the implementation version
	 * @return a string containing the implementation version
	 */
	String getImplementationVersion();
	/**
	 * Gets the compatible Surface Minecraft Version
	 * @return a string containing what Minecraft Version is compatible
	 */
	String getSurfaceMinecraftVersion();
	/**
	 * Gets if the server is on online mode
	 * @return a boolean containing if a server is on online mode
	 */
	boolean getOnlineMode();
	/**
	 * Gets if a server has whitelist enabled
	 * @return a boolean containing if the server's whitelist is enabled
	 */
	boolean isWhiteListEnabled();
	/**
	 * Set the server's whitelist boolean
	 * @param value set whether the server will enable whitelist or disable whitelist
	 */
	void setWhitelistEnabled(boolean value);
	/**
	 * Gets the server's plugin folder.
	 * @return the File object of the plugin folder
	 */
	File getPluginFolder();
	/**
	 * Gets the world of the specified name
	 * @param name the name of the world
	 * @return the world object, null if doesn't exist
	 */
	World getWorld(String name);
	/**
	 * Gets the the player object from UUID
	 * @param uuid the uuid of the player
	 * @return player object, if doesn't exist will return null
	 */
	Player getPlayer(UUID uuid);
	/**
	 * Gets the player object from the player name
	 * @param name the player name
	 * @return the player object, null if doesn't exist
	 * @deprecated Inaccurate, and may cause issues.
	 */
	Player getPlayer(String name);
	/**
	 * Gets the UUID from a playername
	 * @param playerName the playername
	 * @return the UUID of the player name, returns null if player is not online
	 */
	UUID getUUID(String playerName);
	/**
	 * Sends a raw title to the whole server
	 * @param rawCode the raw code for the title (tellraw)
	 */
	void sendRawTitle(String rawCode);
	/**
	 * Sends a title message to the whole server
	 * @param titleMessage the string that will be sent
	 */
	void sendTitle(String titleMessage);
}
