package org.surface.entity;

import java.util.UUID;

import org.surface.literals.GameMode;
import org.surface.literals.PotionType;
import org.surface.literals.Weather;
import org.surface.world.World;

/**
 * Represents a player inside of the server
 * @author PizzaCrust
 *
 */
public interface Player extends EntityBase{
	/**
	 * Sends a message to player
	 * @param message the message that will be sent
	 */
	void sendMessage(String message);
	/**
	 * Gets if the player is dead
	 * @return a boolean containing if the player is dead
	 */
	boolean isDead();
	/**
	 * Gets the gamemode of the player
	 * @return a gamemode enum of the gamemode of the player
	 */
	GameMode getGameMode();
	/**
	 * Set the gamemode of the player
	 * @param gameMode gamemode enum to be on the player
	 */
	void setGameMode(GameMode gameMode);
	/**
	 * Gets the player world's name
	 * @return the world name
	 */
	String getWorldName();
	/**
	 * Gets the player's world object
	 * @return the world object
	 */
	World getWorld();
	/**
	 * Kills the player
	 */
	void kill();
	/**
	 * Adds XP to the player
	 * @param amount the amount of XP
	 */
	void addXp(float amount);
	/**
	 * Gets the total XP of the player
	 * @return amount of XP
	 */
	float getXp();
	/**
	 * Gets the XP in the player's XP bar 
	 * @return amount of XP
	 */
	float getBarXp();
	/**
	 * Removes XP in the player
	 * @param amount amount of XP
	 */
	void removeXp(float amount);
	/**
	 * Gets a array containing the potion effects of the player
	 * @return a array of potion effects
	 */
	PotionType[] getPlayerPotionEffects();
	/**
	 * Adds a potion effect to the player
	 * @param potionType enum the player will be set to
	 */
	void addPotionEffect(PotionType potionType);
	/**
	 * Removes a potion effect on the player
	 * @param potionType enum the player will be set to
	 */
	void removePotionEffect(PotionType potionType);
	/**
	 * Gets the UUID of the player
	 * @return the uuid of the player
	 */
	UUID getUUID();
	/**
	 * Sends a title to the player
	 * @param titleMessage the string that will be on the title
	 */
	void sentTitle(String titleMessage);
	/**
	 * Sends a raw title to the player
	 * @param rawCode the raw code for the title
	 */
	void sendRawTitle(String rawCode);
	/**
	 * Gets the weather of the player's world
	 * @return a enum containing the weather in the player's world
	 */
	Weather getWeather();
	/**
	 * Sets the weather of the player's world
	 * @param weather the weather enum that the world will be set to
	 */
	void setWeather(Weather weather);
}
