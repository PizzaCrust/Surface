package org.surface.world;

import org.surface.Location;
import org.surface.literals.Weather;

/**
 * Represents a world inside of a server.
 * @author PizzaCrust
 *
 */
public interface World {
	/**
	 * Gets the name of the world
	 * @return string of the world name
	 */
	String getName();
	/**
	 * Creates a explosion at location
	 * @param location the location of the explosion
	 */
	void createExplosion(Location location);
	/**
	 * Strikes lightning at location
	 * @param location the location of the lightning strike
	 */
	void strikeLightning(Location location);
	/**
	 * Gets the weather of the world
	 * @return enum containing the current weather
	 */
	Weather getWeather();
}
