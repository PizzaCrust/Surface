package org.surface.entity;

import org.surface.Location;

/**
 * Represents a entity base for entities.
 * @author PizzaCrust
 *
 */
public interface EntityBase {
	/**
	 * Gets the location of the entity
	 * @return the location object of the entity
	 */
	Location getLocation();
	/**
	 * Gets if the entity is living or not
	 * @return a boolean containing if the entity is living or not
	 */
	boolean isLiving();
	/**
	 * Teleports entity to location
	 * @param location the location that the entity will be teleported to
	 */
	void teleport(Location location);
	/**
	 * Teleports entity to entity
	 * @param entity the entity that the entity wll be teleported to
	 */
	void teleport(EntityBase entity);
}
